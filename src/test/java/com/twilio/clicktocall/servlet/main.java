package com.twilio.clicktocall.servlet;

import com.twilio.clicktocall.exceptions.UndefinedEnvironmentVariableException;
import com.twilio.clicktocall.lib.TwilioCallCreator;
import com.twilio.security.RequestValidator;
import org.json.JSONObject;
import org.junit.Before;
import org.mockito.Mock;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import com.twilio.clicktocall.lib.AppSetup;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by Debanga Saikia on 25-07-2019
 */
public class main {
    @Mock
    private static AppSetup appSetup;

    @Mock
    private static TwilioCallCreator twilioCallCreator;
    @Mock
    private static HttpServletRequest request;

    @Mock
    private static HttpServletResponse response;

    @Mock
    private static  ResponseWriter responseWriter;

    @Mock
    private static  RequestValidator requestValidator;


    @Mock
    private static ServletOutputStream outputStream;

    private static CallServlet servlet;

    @Before
    public void setUp() {
        initMocks(this);
    }
    public static void main(String[] args) throws
            ServletException,
            IOException,
            UndefinedEnvironmentVariableException,
            URISyntaxException {

        // Given
      when(request.getRequestURL()).thenReturn(new StringBuffer("http://example.com"));
        when(request.getRequestURI()).thenReturn("/call");
        when(request.getParameter("userNumber")).thenReturn("+919900129549");
        when(request.getParameter("salesNumber")).thenReturn("+12029159007");
        when(response.getOutputStream()).thenReturn(outputStream);
        when(appSetup.getTwilioNumber()).thenReturn("+12029159007");

        servlet = new CallServlet(appSetup, twilioCallCreator);

        // When
        servlet.doPost(request, response);

        // Then
        verify(twilioCallCreator, times(1)).create(
                "twilio-number", "to-phone-number", new URI("http://example.com/connect/sales-phone-number"));

        JSONObject o = new JSONObject();
        o.put("message", "Phone call incoming!");
        o.put("status", "ok");

        verify(outputStream).write(o.toString().getBytes());
    }
}
