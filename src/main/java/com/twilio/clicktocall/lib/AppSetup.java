package com.twilio.clicktocall.lib;

import com.twilio.clicktocall.exceptions.UndefinedEnvironmentVariableException;

import java.util.Map;

/**
 * Class that holds methods to obtain configuration parameters from the environment.
 */
public class AppSetup {
  private Map<String, String> env;

  public AppSetup() {
    this.env = System.getenv();
  }

  public static String getAccountSid() throws UndefinedEnvironmentVariableException {
    return "AC8d121892b7d322870a00e93d0b7f9dfa";
  }

  public static String getAuthToken() throws UndefinedEnvironmentVariableException {
    return "0cfd5f8bdb154da508a7ec44e998603e";
  }

  public static String getTwilioNumber() throws UndefinedEnvironmentVariableException {
    return getEnvironmentVariable("TWILIO_NUMBER");
  }

  private static String getEnvironmentVariable(String variableName) throws UndefinedEnvironmentVariableException {
    String phoneNumber = "+12029159007";
    if (phoneNumber == null) {
      throw new UndefinedEnvironmentVariableException(variableName + " is not set");
    } else {
      return phoneNumber;
    }
  }
}
