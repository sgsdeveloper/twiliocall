package com.twilio.clicktocall.servlet;


import com.twilio.clicktocall.exceptions.UndefinedEnvironmentVariableException;
import com.twilio.clicktocall.lib.AppSetup;
import com.twilio.clicktocall.lib.TwilioCallCreator;
import com.twilio.exception.TwilioException;
import com.twilio.http.TwilioRestClient;
import com.twilio.rest.api.v2010.account.Call;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.List;


@WebServlet("/call")
public class CallServlet extends HttpServlet {
    private final TwilioCallCreator twilioCallCreator;
    private final String twilioNumber;

    @SuppressWarnings("unused")
    public CallServlet()
            throws UndefinedEnvironmentVariableException {
      //  AppSetup appSetup = new AppSetup();
        TwilioRestClient client = new TwilioRestClient.Builder(
                AppSetup.getAccountSid(), AppSetup.getAuthToken()).build();

        this.twilioNumber = AppSetup.getTwilioNumber();
        this.twilioCallCreator = new TwilioCallCreator(client);
    }

    public CallServlet(AppSetup appSetup, TwilioCallCreator twilioCallCreator)
            throws UndefinedEnvironmentVariableException {
        this.twilioNumber = AppSetup.getTwilioNumber();
        this.twilioCallCreator = twilioCallCreator;
    }

    /**
     * Method that triggers a call to the specified number in the request
     * @param request incoming servlet request object
     * @param response servlet response object
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String agentNumber = request.getParameter("agentNumber");
        String customerNumber = request.getParameter("customerNumber");
          System.out.println(agentNumber+" agentNumber++customerNumber "+customerNumber);
      //  String agentNumber = "+919900129549";
       // String customerNumber = "+919060322507";
        System.out.println(agentNumber+" doPost "+customerNumber);
        if (agentNumber == null || customerNumber == null) {
            response.getOutputStream()
                    .write(getJSONResponse("Both phone numbers need to be provided").getBytes());
            return;
        }

        // Full URL to the end point that will respond with the call TwiML.
        String  encodedcustomerNumber= URLEncoder.encode(customerNumber, "UTF-8");
        System.out.println("encodedcustomerNumber : "+ encodedcustomerNumber);
        String url = request.getRequestURL().toString().replace(request.getRequestURI(), "");
        url += "/connect/" + encodedcustomerNumber;
        System.out.println("url: "+url);
        String callSid = "";
        try {
            Call call = twilioCallCreator.create(this.twilioNumber, agentNumber, new URI(url));
            callSid = call.getSid();
            System.out.println("Call Sid : "+callSid);


        } catch (Exception e) {
            e.printStackTrace();
            String message =
                    "Twilio rest client error: " + e.getMessage() + "\n" +
                            "Remember not to use localhost to access this app, use your ngrok URL";

            response.getOutputStream()
                    .write(getJSONResponse(message).getBytes());
        }

        response.getOutputStream()
                .write(getJSONResponse(callSid).getBytes());
    }

    private String getJSONResponse(String message) {
        JSONObject obj = new JSONObject();
        obj.put("message", message);
        obj.put("status", "ok");

        return obj.toString();
    }
}
