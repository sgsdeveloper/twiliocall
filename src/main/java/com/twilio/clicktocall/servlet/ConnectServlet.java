package com.twilio.clicktocall.servlet;

import com.twilio.clicktocall.exceptions.UndefinedEnvironmentVariableException;
import com.twilio.clicktocall.lib.AppSetup;
import com.twilio.security.RequestValidator;
import com.twilio.twiml.VoiceResponse;
import com.twilio.twiml.voice.Say;
import com.twilio.twiml.voice.Dial;
import com.twilio.twiml.voice.Number;
import com.twilio.twiml.TwiMLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/connect/*")
public class ConnectServlet extends HttpServlet {
    private RequestValidator requestValidator;
    private ResponseWriter responseWriter;

    @SuppressWarnings("unused")
    public ConnectServlet() throws UndefinedEnvironmentVariableException {
        this.responseWriter = new ResponseWriter();
        this.requestValidator = new RequestValidator(AppSetup.getAuthToken());
    }

    public ConnectServlet(RequestValidator requestValidator, ResponseWriter responseWriter) {
        this.requestValidator = requestValidator;
        this.responseWriter = responseWriter;
    }

    /**
     * Method that handles /connect request and responds with the TwiML after validating
     * the authenticity of the request
     *
     * @param request  incoming servlet request object
     * @param response servlet response object
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println(" inside do post of connect ");
        if (isValidRequest(request)) {
            String customerNumber = extractCustomerNumber(request);
            System.out.println(" inside do post of connect - customerNumber  "+customerNumber);
            String r = getXMLResponse(customerNumber);
            responseWriter.writeIn(response, r);
        } else {
            System.out.println(" inside do post of connect --invalid twilio request ");
            responseWriter.writeIn(response, "Invalid twilio request");
        }
    }

    /**
     * Generates the TwiML with a Say and Hangout verb
     *
     * @param customerNumber
     * @return String with the TwiML
     */
    private String getXMLResponse(String customerNumber) {
        Number number = new Number.Builder(customerNumber).build();
        Dial dial = new Dial.Builder()
                .number(number)
                .build();
        VoiceResponse response = new VoiceResponse.Builder()
                .say(new Say.Builder("Please wait while we connect your phone to customer").build())
                .dial(dial)
                .build();

        try {
            return response.toXml();
        } catch (TwiMLException e) {
            e.printStackTrace();
        }

        return "";
    }


    /**
     * Uses TwilioUtils to validate that the incoming request comes from Twilio automated services
     *
     * @param request passed servlet request to extract parameters necessary for validation
     * @return boolean determining validity of the request
     */
    private boolean isValidRequest(HttpServletRequest request) {
        String url = request.getRequestURL().toString();
        Map<String, String> params = new HashMap<>();

        Enumeration<String> names = request.getParameterNames();
        while (names.hasMoreElements()) {
            String currentName = names.nextElement();
            params.put(currentName, request.getParameter(currentName));
        }

        String signature = request.getHeader("X-Twilio-Signature");

        return requestValidator.validate(url, params, signature);
    }

    /**
     * Returns the customer number that was passed as a path parameter
     * @param request
     * @return
     */
    private String extractCustomerNumber(HttpServletRequest request) {
        String[] pathElements = request.getPathInfo().split("/");
        if(pathElements.length > 0) {
            return pathElements[pathElements.length - 1];
        }
        return "";
    }

}
