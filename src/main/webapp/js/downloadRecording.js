// Execute JavaScript on page load
$(function() {
       var $form = $('#contactform'),
        $submit = $('#contactform input[type=submit]');

    // Intercept form submission
    $form.on('submit', function(e) {
	    var x = document.getElementById("userNumber").value;
		alert("val "+x);
        // Prevent form submission and repeat clicks
        e.preventDefault();
        $submit.attr('disabled', 'disabled');

        // Submit the form via ajax
        $.ajax({
            url:'https://api.twilio.com/2010-04-01/Accounts/AC8d121892b7d322870a00e93d0b7f9dfa/Recordings/RE0eaba2dbf565f8da8e0076e87e12fbfe.mp3',
            method:'GET',
            data: $form.serialize()
        }).done(function(data) {
            alert('Downloaded mp3');
            content = JSON.parse(data);
            alert(content.message);
        }).fail(function() {
            alert('There was a problem in downloading.');
        }).always(function() {
            $submit.removeAttr('disabled');
        });

    });
});
