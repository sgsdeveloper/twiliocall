// Execute JavaScript on page load
$(function() {
    $('#agentNumber, #customerNumber').intlTelInput({
        responsiveDropdown: true,
        autoFormat: true
    });
    var $form = $('#contactform'),
        $submit = $('#contactform input[type=submit]');

    // Intercept form submission
    $form.on('submit', function(e) {
        // Prevent form submission and repeat clicks
        e.preventDefault();
        $submit.attr('disabled', 'disabled');
        var agentNumber = document.getElementById("agentNumber").value;
        var customerNumber = document.getElementById("customerNumber").value;
     //   var agentNumber = $('#contactform').find('input[name="agentNumber"]').val();
      //  var customerNumber= $('#contactform').find('input[name="customerNumber"]').val();
        alert(agentNumber+' trying to connect  to1  '+customerNumber);
        // Submit the form via ajax
        $.ajax({
            url:'/call',
            method:'POST',
            // url:'https://api.twilio.com/2010-04-01/Accounts/AC8d121892b7d322870a00e93d0b7f9dfa/Recordings/RE0eaba2dbf565f8da8e0076e87e12fbfe.mp3',
             // method:'GET',
            data: $form.serialize()
        }).done(function(data) {
            content = JSON.parse(data);
            alert(content.message);
        }).fail(function() {
            alert('There was a problem calling you - please try again later.');
        }).always(function() {
            $submit.removeAttr('disabled');
        });

    });
});
